FROM node:alpine3.17

WORKDIR /opt/app

COPY package*.json ./

RUN apk add --no-cache curl && \
    npm ci --production

COPY --chown=node:node src .

USER node

EXPOSE 80

CMD [ "node", "app.js" ]